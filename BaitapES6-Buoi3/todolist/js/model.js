export class TodoList {
    constructor(isCompleted, activity, id) {
        this.isCompleted = isCompleted;
        this.activity = activity;
        this.id = id;
    }
}