import { layThongTin, renderTaskcompleted, renderTaskUncompleted, filterTaskUnCompleted, filterTaskCompleted, sortAZ, sortZA } from "./controller.js"
import { TodoList } from "./model.js"

const BASE_URL = 'https://62f8b754e0564480352bf3cc.mockapi.io'


function renderTaskServices() {
    axios({
        url: `${BASE_URL}/todolist`,
        method: 'GET',
    })
        .then(res => {
            let dataService = res.data


            // filter task chưa hoàn thành
            let taskUnCompleted = filterTaskUnCompleted(dataService)
            renderTaskUncompleted(taskUnCompleted)

            // filter task đã hoàn thành
            let taskCompleted = filterTaskCompleted(dataService)
            renderTaskcompleted(taskCompleted)
        })
        .catch(err => {
            console.log('err: ', err);

        })
}
renderTaskServices()


window.addActivity = addActivity
function addActivity() {
    if (document.getElementById("newTask").value == '') {
        alert('Bạn chưa nhập công việc!!!')
        return
    }
    let { activity } = layThongTin()
    let newTask = new TodoList(false, activity)
    axios({
        url: `${BASE_URL}/todolist`,
        method: 'POST',
        data: newTask
    })
        .then(res => {
            renderTaskServices()

        })
        .catch(err => {
            console.log('err: ', err);

        })
    document.getElementById("newTask").value = ''

}

window.deleteTask = deleteTask
function deleteTask(id) {

    axios({
        url: `${BASE_URL}/todolist/${id}`,
        method: 'DELETE',
    })
        .then(res => {
            renderTaskServices()

        })
        .catch(err => {
            console.log('err: ', err);

        })
}

window.isChecked = isChecked
function isChecked(id, isChecked) {

    let newTask = new TodoList(isChecked)
    axios({
        url: `${BASE_URL}/todolist/${id}`,
        method: 'PUT',
        data: newTask
    })
        .then(res => {
            renderTaskServices()

        })
        .catch(err => {
            console.log('err: ', err);

        })
}


window.sortActivity = sortActivity
function sortActivity(sort) {

    axios({
        url: `${BASE_URL}/todolist`,
        method: 'GET',
    })
        .then(res => {
            let dataService = res.data

            if (sort === true) {
                dataService.sort(sortAZ)
            } else {
                dataService.sort(sortZA)
            }
            // filter task chưa hoàn thành
            let taskUnCompleted = filterTaskUnCompleted(dataService)
            renderTaskUncompleted(taskUnCompleted)

            // filter task đã hoàn thành
            let taskCompleted = filterTaskCompleted(dataService)
            renderTaskcompleted(taskCompleted)
        })
        .catch(err => {
            console.log('err: ', err);

        })


}


