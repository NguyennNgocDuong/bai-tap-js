export const layThongTin = () => {
  const activity = document.getElementById("newTask").value;
  return {
    activity,
  };
};

export const renderTaskUncompleted = (list) => {
  let content = "";
  list.forEach((task) => {
    content += `
          <li>${task.activity}
          <div class="action">
          <button onclick='deleteTask(${
            task.id
          })'  style="border: none;cursor:pointer"><i class="fa fa-trash"></i></button>
          <button id="btnChecked" onclick='isChecked(${
            task.id
          },${true})' style="border: none;cursor:pointer"><i class="fa fa-check-circle"></i></button>
          </div>
          </li>
          `;
  });
  document.getElementById("todo").innerHTML = content;
};

export const renderTaskcompleted = (list) => {
  let content = "";
  list.forEach((task) => {
    content += `
          <li>${task.activity}
          <div class="action">
          <button onclick='deleteTask(${
            task.id
          })'  style="border: none;cursor:pointer"><i class="fa fa-trash"></i></button>
          <button id="btnChecked" onclick='isChecked(${
            task.id
          },${false})' style="border: none;cursor:pointer;color:green"><i class="fa fa-check-circle"></i></button>
          </div>
          </li>
          `;
  });
  document.getElementById("completed").innerHTML = content;
};

export const filterTaskUnCompleted = (list) => {
  return list.filter((task) => {
    return task.isCompleted === false;
  });
};

export const filterTaskCompleted = (list) => {
  return list.filter((task) => {
    return task.isCompleted === true;
  });
};

export const sortZA = (a, b) => {
  if (a.activity.toLowerCase() < b.activity.toLowerCase()) {
    return -1;
  } else if (a.activity.toLowerCase() > b.activity.toLowerCase()) {
    return 1;
  } else {
    return 0;
  }
};

export const sortAZ = (a, b) => {
  if (a.activity.toLowerCase() < b.activity.toLowerCase()) {
    return 1;
  } else if (a.activity.toLowerCase() > b.activity.toLowerCase()) {
    return -1;
  } else {
    return 0;
  }
};
