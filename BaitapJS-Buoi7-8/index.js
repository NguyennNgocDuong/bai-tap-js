var array = []

function domId(id) {
    return document.getElementById(id)
}

function pushArray() {
    event.preventDefault()
    if (domId('soN').value == '') {
        return
    }
    array.push(domId("soN").value * 1);
    domId('box').innerHTML = `[${array}]`
    domId("soN").value = ''

}

function tinhTongDuong() {
    var sum = 0

    for (var i = 0; i < array.length; i++) {
        if (array[i] > 0) {
            sum += array[i]

        }
    }
    domId('tongSoDuong').innerHTML = `<h3>Tổng số dương là: ${sum}</h3>`
}

function demSoDuong() {
    var count = 0
    for (var i = 0; i < array.length; i++) {
        if (array[i] > 0) {
            count++
        }
    }
    domId('demSoDuong').innerHTML = `<h3>Có ${count} số dương</h3>`

}

function timSoNhoNhat() {
    var min = array[0]
    for (var i = 0; i < array.length; i++) {
        if (array[i] < min) {
            min = array[i]

        }
    }
    domId('timSoNhoNhat').innerHTML = `<h3>Số nhỏ nhất: ${min}</h3>`

}

function timSoDuongNhoNhat() {

    var newArray = array.filter(n => {

        return n > 0
    })

    if (newArray.length > 0) {
        var min = newArray[0]
        for (var i = 0; i < newArray.length; i++) {
            if (newArray[i] < min) {
                min = newArray[i]
            }
        }
        domId('timSoDuongNhoNhat').innerHTML = `<h3>Số dương nhỏ nhất: ${min}</h3>`
    } else {
        domId('timSoDuongNhoNhat').innerHTML = `<h3>Không có số dương nhỏ nhất</h3>`

    }

}

function timSoChanCuoiCung() {
    var sochan = -1
    for (var i = 0; i < array.length; i++) {
        if (array[i] % 2 == 0) {
            sochan = array[i]

        }
    }
    domId('timSoChanCuoiCung').innerHTML = `<h3>Số chẵn cuối cùng: ${sochan}</h3>`

}

function doiCho() {
    event.preventDefault()
    var so1 = domId('so1').value * 1
    var so2 = domId('so2').value * 1

    var newArray = [array[so1], array[so2]] = [array[so2], array[so1]]
    domId('doiCho').innerHTML = `<h3>Mảng sau khi đổi: [${array}]</h3>`


}

function sapXep() {
    array.sort(function (a, b) {
        return a - b
    })
    domId('sapXep').innerHTML = `<h3>Mảng sau khi sắp xếp: [${array}]</h3>`

}

function isSoNguyenTo(n) {


    var soNguyenTo = true;

    if (n < 2) {
        return soNguyenTo = false;
    }


    for (var i = 2; i < n; i++) {
        if (n % i == 0) {
            soNguyenTo = false;
            break
        }
    }
    return soNguyenTo;

}

function timSoNguyenToDauTien() {
    for (var i = 0; i < array.length; i++) {
        if (isSoNguyenTo(array[i]) == true) {
            domId('timSoNguyenToDauTien').innerHTML = `<h3>${array[i]}</h3>`

            break
        } else {
            domId('timSoNguyenToDauTien').innerHTML = `<h3>-1</h3>`
        }

    }


}


function demSoNguyen() {
    count = 0
    for (var i = 0; i < array.length; i++) {
        if (Number.isInteger(array[i]) == true) {
            count++
        }
    }
    domId('demSoNguyen').innerHTML = `<h3>Có ${count} số nguyên</h3>`

}



function soSanhSoAmDuong() {

    var soAm = 0
    var soDuong = 0

    for (var i = 0; i < array.length; i++) {
        if (array[i] > 0) {
            soDuong++
        } else {
            soAm++
        }
    }
    if (soAm > soDuong) {
        domId('soSanhSoAmDuong').innerHTML = `<h3>Số âm > số dương</h3>`

    } else if (soAm < soDuong) {
        domId('soSanhSoAmDuong').innerHTML = `<h3>Số âm < số dương</h3>`

    } else {
        domId('soSanhSoAmDuong').innerHTML = `<h3>Số âm = số dương</h3>`

    }

}