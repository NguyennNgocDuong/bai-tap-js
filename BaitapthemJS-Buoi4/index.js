// Bài 1

function tinhNgayHomQua() {

    event.preventDefault()
    var day = document.getElementById('ngay').value * 1
    var month = document.getElementById('thang').value * 1
    var year = document.getElementById('nam').value * 1
    var ngayThangNam = document.getElementById('ngaythangnam')

    switch (month) {
        case 1: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`

            } else if (day == 1) {
                ngayThangNam.innerHTML = `<h2 class="text-success">31/12/${year - 1}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day - 1}/${month}/${year}</h2>`

            }
            break

        }
        case 2: {
            if (day > 28) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            } else if (day == 1) {
                ngayThangNam.innerHTML = `<h2 class="text-success">31/${month - 1}/${year - 1}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day - 1}/${month}/${year}</h2>`

            }
            break

        } case 3: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            } else if (day == 1) {
                ngayThangNam.innerHTML = `<h2 class="text-success">28/${month - 1}/${year - 1}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day - 1}/${month}/${year}</h2>`

            }
            break

        } case 4: {
            if (day > 30) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            } else if (day == 1) {
                ngayThangNam.innerHTML = `<h2 class="text-success">31/${month - 1}/${year - 1}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day - 1}/${month}/${year}</h2>`

            }
            break

        } case 5: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            } else if (day == 1) {
                ngayThangNam.innerHTML = `<h2 class="text-success">30/${month - 1}/${year - 1}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day - 1}/${month}/${year}</h2>`

            }
            break

        } case 6: {
            if (day > 30) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            } else if (day == 1) {
                ngayThangNam.innerHTML = `<h2 class="text-success">31/${month - 1}/${year - 1}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day - 1}/${month}/${year}</h2>`

            }
            break

        } case 7: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            } else if (day == 1) {
                ngayThangNam.innerHTML = `<h2 class="text-success">30/${month - 1}/${year - 1}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day - 1}/${month}/${year}</h2>`

            }
            break

        } case 8: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            } else if (day == 1) {
                ngayThangNam.innerHTML = `<h2 class="text-success">31/${month - 1}/${year - 1}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day - 1}/${month}/${year}</h2>`

            }
            break

        } case 9: {
            if (day > 30) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            } else if (day == 1) {
                ngayThangNam.innerHTML = `<h2 class="text-success">31/${month - 1}/${year - 1}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day - 1}/${month}/${year}</h2>`

            }
            break

        } case 10: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            } else if (day == 1) {
                ngayThangNam.innerHTML = `<h2 class="text-success">30/${month - 1}/${year - 1}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day - 1}/${month}/${year}</h2>`

            }
            break

        } case 11: {
            if (day > 30) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            } else if (day == 1) {
                ngayThangNam.innerHTML = `<h2 class="text-success">31/${month - 1}/${year - 1}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day - 1}/${month}/${year}</h2>`

            }
            break

        } case 12: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            } else if (day == 1) {
                ngayThangNam.innerHTML = `<h2 class="text-success">30/${month - 1}/${year - 1}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day - 1}/${month}/${year}</h2>`

            }
            break
        }
    }
}
function tinhNgayMai() {

    event.preventDefault()
    var day = document.getElementById('ngay').value * 1
    var month = document.getElementById('thang').value * 1
    var year = document.getElementById('nam').value * 1
    var ngayThangNam = document.getElementById('ngaythangnam')

    switch (month) {
        case 1: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`

            } else if (day == 31) {
                ngayThangNam.innerHTML = `<h2 class="text-success">1/${month + 1}/${year}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day + 1}/${month}/${year}</h2>`

            }
            break

        }
        case 2: {
            if (day > 28) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            }
            else if (day == 28) {
                ngayThangNam.innerHTML = `<h2 class="text-success">1/${month + 1}/${year}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day + 1}/${month}/${year}</h2>`

            }
            break

        } case 3: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            }
            else if (day == 31) {
                ngayThangNam.innerHTML = `<h2 class="text-success">1/${month + 1}/${year}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day + 1}/${month}/${year}</h2>`

            }
            break

        } case 4: {
            if (day > 30) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            }
            else if (day == 30) {
                ngayThangNam.innerHTML = `<h2 class="text-success">1/${month + 1}/${year}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day + 1}/${month}/${year}</h2>`

            }
            break

        } case 5: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            }
            else if (day == 31) {
                ngayThangNam.innerHTML = `<h2 class="text-success">1/${month + 1}/${year}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day + 1}/${month}/${year}</h2>`

            }
            break

        } case 6: {
            if (day > 30) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            }
            else if (day == 30) {
                ngayThangNam.innerHTML = `<h2 class="text-success">1/${month + 1}/${year}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day + 1}/${month}/${year}</h2>`

            }
            break

        } case 7: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            }
            else if (day == 31) {
                ngayThangNam.innerHTML = `<h2 class="text-success">1/${month + 1}/${year}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day + 1}/${month}/${year}</h2>`

            }
            break

        } case 8: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            }
            else if (day == 31) {
                ngayThangNam.innerHTML = `<h2 class="text-success">1/${month + 1}/${year}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day + 1}/${month}/${year}</h2>`

            }
            break

        } case 9: {
            if (day > 30) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            }
            else if (day == 30) {
                ngayThangNam.innerHTML = `<h2 class="text-success">1/${month + 1}/${year}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day + 1}/${month}/${year}</h2>`

            }
            break

        } case 10: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            }
            else if (day == 31) {
                ngayThangNam.innerHTML = `<h2 class="text-success">1/${month + 1}/${year}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day + 1}/${month}/${year}</h2>`

            }
            break

        } case 11: {
            if (day > 30) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            }
            else if (day == 30) {
                ngayThangNam.innerHTML = `<h2 class="text-success">1/${month + 1}/${year}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day + 1}/${month}/${year}</h2>`

            }
            break

        } case 12: {
            if (day > 31) {
                ngayThangNam.innerHTML = `<h2 class="text-danger">Ngày không hợp lệ</h2>`
            }
            else if (day == 31) {
                ngayThangNam.innerHTML = `<h2 class="text-success">1/1/${year + 1}</h2>`

            } else {
                ngayThangNam.innerHTML = `<h2 class="text-success">${day + 1}/${month}/${year}</h2>`

            }
            break
        }
    }
}


// Bài 2
function tinhNgay() {
    event.preventDefault()
    var thang = document.getElementById('nhapthang').value * 1
    var nam = document.getElementById('nhapnam').value * 1
    var result = document.getElementById('tinhngay')

    switch (thang) {
        case 1: {
            result.innerHTML = `<h2>Tháng ${thang} năm ${nam} có 31 ngày</h2>`
            break
        }
        case 2: {
            if (nam % 4 == 0 && nam % 100 > 0 || nam % 400 == 0) {
                result.innerHTML = `<h2>Tháng ${thang} năm ${nam} có 29 ngày</h2>`

            } else {
                result.innerHTML = `<h2>Tháng ${thang} năm ${nam} có 28 ngày</h2>`

            }
            break
        } case 3: {
            result.innerHTML = `<h2>Tháng ${thang} năm ${nam} có 31 ngày</h2>`
            break
        } case 4: {
            result.innerHTML = `<h2>Tháng ${thang} năm ${nam} có 30 ngày</h2>`
            break
        } case 5: {
            result.innerHTML = `<h2>Tháng ${thang} năm ${nam} có 31 ngày</h2>`
            break
        } case 6: {
            result.innerHTML = `<h2>Tháng ${thang} năm ${nam} có 30 ngày</h2>`
            break
        } case 7: {
            result.innerHTML = `<h2>Tháng ${thang} năm ${nam} có 31 ngày</h2>`
            break
        } case 8: {
            result.innerHTML = `<h2>Tháng ${thang} năm ${nam} có 31 ngày</h2>`
            break
        } case 9: {
            result.innerHTML = `<h2>Tháng ${thang} năm ${nam} có 30 ngày</h2>`
            break
        } case 10: {
            result.innerHTML = `<h2>Tháng ${thang} năm ${nam} có 31 ngày</h2>`
            break
        } case 11: {
            result.innerHTML = `<h2>Tháng ${thang} năm ${nam} có 30 ngày</h2>`
            break
        } case 12: {
            result.innerHTML = `<h2>Tháng ${thang} năm ${nam} có 31 ngày</h2>`
            break
        }
    }

}

// Bài 3
function docSo() {
    event.preventDefault()
    var soco3chuso = document.getElementById('soco3chuso').value * 1
    var result = document.getElementById('docso')
    var tram = Math.floor(soco3chuso / 100)
    console.log('tram: ', tram);
    var chuc = (Math.floor(soco3chuso / 10)) % 10
    console.log('chuc: ', chuc);
    var donvi = soco3chuso % 10
    console.log('donvi: ', donvi);
    var ketqua = ""
    if (tram == 0 || tram > 9) {
        return result.innerHTML = `<h2>Số không hợp lệ</h2>`
    }
    switch (chuc) {

        case 2: {
            chuc = "hai"
            break
        } case 3: {
            chuc = "ba"
            break
        } case 4: {
            chuc = "bốn"
            break
        } case 5: {
            chuc = "năm"
            break
        } case 6: {
            chuc = "sáu"
            break
        } case 7: {
            chuc = "bảy"
            break
        } case 8: {
            chuc = "tám"
            break
        } case 9: {
            chuc = "chín"
            break
        }
    }


    switch (tram) {
        case 1: {
            tram = "Một"
            break
        }
        case 2: {
            tram = "Hai"
            break
        } case 3: {
            tram = "Ba"
            break
        } case 4: {
            tram = "Bốn"
            break
        } case 5: {
            tram = "Năm"
            break
        } case 6: {
            tram = "Sáu"
            break
        } case 7: {
            tram = "Bảy"
            break
        } case 8: {
            tram = "Tám"
            break
        } case 9: {
            tram = "Chín"
            break
        }
    }

    switch (donvi) {

        case 2: {
            donvi = "hai"
            break
        } case 3: {
            donvi = "ba"
            break
        } case 4: {
            donvi = "bốn"
            break
        } case 5: {
            donvi = "năm"
            break
        } case 6: {
            donvi = "sáu"
            break
        } case 7: {
            donvi = "bảy"
            break
        } case 8: {
            donvi = "tám"
            break
        } case 9: {
            donvi = "chín"
            break
        }
    }


    // xét số hàng trăm
    if (tram != 0) {
        ketqua += tram + " trăm "
        if ((chuc == 0) && (donvi != 0)) {
            ketqua += " linh ";
        }
    }
    // xét số hàng chục
    if ((chuc != 0) && (chuc != 1)) {
        ketqua += chuc + " mươi ";
    }
    if (chuc == 1) ketqua += " mười ";
    // xét hàng đơn vị
    switch (donvi) {
        case 1:
            if ((chuc != 0) && (chuc != 1)) {
                ketqua += " mốt ";
            }

            else {
                ketqua += "một";
            }
            break;
        case 5:
            if (chuc == 0) {
                ketqua += donvi;
            }
            else {
                ketqua += " lăm ";
            }
            break;
        default:
            if (donvi != 0) {
                ketqua += donvi;
            }
            break;
    }
    return result.innerHTML = `<h2>${ketqua}</h2>`
        ;


}

// Bài 4
function timSinhVien() {
    event.preventDefault()
    var tensv1 = document.getElementById('tensv1').value;
    var tensv2 = document.getElementById('tensv2').value;
    var tensv3 = document.getElementById('tensv3').value;

    var x1 = document.getElementById('x1').value * 1;
    var x2 = document.getElementById('x2').value * 1;
    var x3 = document.getElementById('x3').value * 1;
    var x4 = document.getElementById('x4').value * 1;

    var y1 = document.getElementById('y1').value * 1
    var y2 = document.getElementById('y2').value * 1
    var y3 = document.getElementById('y3').value * 1
    var y4 = document.getElementById('y4').value * 1

    var result = document.getElementById('timsinhvien')

    if (Math.abs((x4 + y4) - (x1 + y1)) > Math.abs((x4 + y4) - (x2 + y2)) && Math.abs((x4 + y4) - (x1 + y1)) > Math.abs((x4 + y4) - (x3 + y3))) {
        result.innerHTML = `<h2>Sinh viên xa trường nhất : ${tensv1}</h2>`
    } else if (Math.abs((x4 + y4) - (x2 + y2)) > Math.abs((x4 + y4) - (x1 + y1)) && Math.abs((x4 + y4) - (x2 + y2)) > Math.abs((x4 + y4) - (x3 + y3))) {
        result.innerHTML = `<h2>Sinh viên xa trường nhất : ${tensv2}</h2>`

    } else {
        result.innerHTML = `<h2>Sinh viên xa trường nhất : ${tensv3}</h2>`

    }
}




