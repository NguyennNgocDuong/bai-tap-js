// Bài 1
function sapXep() {
    event.preventDefault()
    var soThuNhat = document.getElementById("soThuNhat").value * 1;
    console.log('soThuNhat: ', soThuNhat);
    var soThuHai = document.getElementById("soThuHai").value * 1;
    console.log('soThuHai: ', soThuHai);
    var soThuBa = document.getElementById("soThuBa").value * 1;
    console.log('soThuBa: ', soThuBa);
    var result = document.getElementById("sapxep")

    if (soThuNhat > soThuHai && soThuNhat > soThuBa && soThuHai > soThuBa) {
        result.innerHTML = `<h2>${soThuBa} < ${soThuHai} < ${soThuNhat}</h2>`
    } else if (soThuNhat > soThuHai && soThuHai < soThuBa && soThuNhat > soThuBa) {
        result.innerHTML = `<h2>${soThuHai} < ${soThuBa} < ${soThuNhat}</h2>`
    } else if (soThuNhat > soThuHai && soThuNhat < soThuBa) {
        result.innerHTML = `<h2>${soThuHai} < ${soThuNhat} < ${soThuBa}</h2>`
    } else if (soThuNhat < soThuHai && soThuHai > soThuBa && soThuNhat > soThuBa) {
        result.innerHTML = `<h2>${soThuBa} < ${soThuNhat} < ${soThuHai}</h2>`
    } else if (soThuHai > soThuBa && soThuNhat < soThuBa) {
        result.innerHTML = `<h2>${soThuNhat} < ${soThuBa} < ${soThuHai}</h2>`
    } else {
        result.innerHTML = `<h2>${soThuNhat} < ${soThuHai} < ${soThuBa}</h2>`

    }
}

// Bài 2
function guiLoiChao() {
    event.preventDefault()
    var option = document.getElementById('option').value * 1
    console.log('option: ', option);
    var result = document.getElementById('guiloichao')
    console.log('result: ', result);
    switch (option) {
        case 1: {
            result.innerHTML = `<h2>Xin chào Người lạ ơi!</h2>`
            break;
        } case 2: {
            result.innerHTML = `<h2>Xin chào Bố!</h2>`
            break;

        } case 3: {
            result.innerHTML = `<h2>Xin chào Mẹ!</h2>`
            break;

        } case 4: {
            result.innerHTML = `<h2>Xin chào Anh trai!</h2>`
            break;

        } case 5: {
            result.innerHTML = `<h2>Xin chào Em gái!</h2>`
            break;

        }
    }
}

// Bài 3
function demSo() {
    event.preventDefault();
    var sothunhat = document.getElementById('sothunhat').value * 1
    var sothuhai = document.getElementById('sothuhai').value * 1
    var sothuba = document.getElementById('sothuba').value * 1
    var result = document.getElementById('demso')
    var count = 0
    var sole = 0

    if (sothunhat % 2 == 0) {
        count += 1
    }
    if (sothuhai % 2 == 0) {
        count += 1
    }
    if (sothuba % 2 == 0) {
        count += 1
    }

    sole = 3 - count
    console.log('sole: ', sole);

    console.log('count: ', count);
    result.innerHTML = `<h2>Có ${count} số chẵn, ${sole} số lẻ </h2>`
}

// Bài 4
function doanHinhTamGiac() {
    event.preventDefault()
    var canhthu1 = document.getElementById('canhthu1').value * 1
    var canhthu2 = document.getElementById('canhthu2').value * 1
    var canhthu3 = document.getElementById('canhthu3').value * 1
    var result = document.getElementById('hinhtamgiac')


    if (canhthu1 == canhthu2 && canhthu2 == canhthu3) {
        result.innerHTML = `<h2>Hình tam giác đều</h2>`
    }
    else if (
        canhthu1 == canhthu2 ||
        canhthu1 == canhthu3 ||
        canhthu2 == canhthu3) {
        result.innerHTML = `<h2>Hình tam giác cân</h2>`
    }
    else if (
        Math.pow(canhthu1, 2) == Math.pow(canhthu2, 2) + Math.pow(canhthu3, 2) ||
        Math.pow(canhthu2, 2) == Math.pow(canhthu1, 2) + Math.pow(canhthu3, 2) ||
        Math.pow(canhthu3, 2) == Math.pow(canhthu1, 2) + Math.pow(canhthu2, 2)
    ) {
        result.innerHTML = `<h2>Hình tam giác vuông</h2>`
    } else {
        result.innerHTML = `<h2>Một hình tam giác nào đó</h2>`
    }
}



