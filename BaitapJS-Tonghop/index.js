function domId(id) {
    return document.getElementById(id)
}

// Bài 1


function inSo1To100() {
    event.preventDefault()


    var tagTable = document.createElement('table')


    for (var i = 1; i <= 100; i += 10) {
        var tagTr = document.createElement('tr')
        tagTable.appendChild(tagTr)
        for (var j = i; j < i + 10; j++) {
            var tagTd = document.createElement('td')
            tagTd.style.padding = '5px'
            var tagH3 = document.createElement('h3')
            var textNode = document.createTextNode(j)
            tagTd.appendChild(tagH3)
            tagH3.appendChild(textNode)
            tagTr.appendChild(tagTd)


        }

        domId('inSo1To100').appendChild(tagTable)
    }





}

// Bài 2
var arr = []
function pushArray() {
    event.preventDefault()
    if (domId('txt_number').value == '') {
        return
    }
    var number = domId('txt_number').value * 1
    arr.push(number)
    domId('array').innerHTML = `[${arr}]`
    domId('txt_number').value = ''
}

function isSoNguyenTo(n) {


    var soNguyenTo = true;

    if (n < 2) {
        return soNguyenTo = false;
    }


    for (var i = 2; i < n; i++) {
        if (n % i == 0) {
            soNguyenTo = false;
            break
        }
    }
    return soNguyenTo;

}

function inSoNguyenTo() {
    var soNguyenTo = 0
    for (var i = 0; i < arr.length; i++) {
        if (isSoNguyenTo(arr[i]) == true) {
            soNguyenTo += arr[i] + " "
            domId('timSoNguyenTo').innerHTML = `<h3>${soNguyenTo}</h3>`

        } else {
            domId('timSoNguyenTo').innerHTML = `<h3>Không có số nguyên tố</h3>`
        }

    }


}

// Bài 3

function tinhTong() {
    event.preventDefault()
    var n = document.getElementById('txtnumber').value * 1
    var sum = 0
    for (var i = 2; i <= n; i++) {
        sum += (2 * i)
    }
    document.getElementById('tinhTongBai3').innerHTML = sum
}

// Bài 4
function tinhLuongUocSo() {

    var n = document.getElementById('inputLuongUocSo').value * 1
    var luongSo = ''
    for (var i = 0; i <= n; i++) {
        if (n % i == 0) {
            luongSo += i + " "


        }
    }
    document.getElementById('luongUocSo').innerHTML = `<h3>Ước số của ${n} là: ${luongSo}</h3 > `
}

// Bài 5
function timSoDaoNguoc() {
    var n = String(document.getElementById('inputSoDaoNguoc').value * 1)
    // split chuyển chuỗi thành mảng
    // reverse đảo phần tử mảng
    // join chuyển mảng thành chuỗi
    var soDaoNguoc = n.split('').reverse().join('')
    document.getElementById('soDaoNguoc').innerHTML = `<h3>Số đảo ngược của ${n} là: ${soDaoNguoc}</h3>`

}

// Bài 6
function timSoNguyenDuongLonNhat() {
    var sum = 0
    var count = 0
    for (var i = 0; i <= 100; i++) {
        count++
        sum += count
    }
    document.getElementById('soNguyenDuong').innerHTML = `<h3>Số nguyên dương lớn nhất: ${sum}</h3>`
}

// Bài 7
function tinhBangCuuChuong() {
    var n = document.getElementById('inputBangCuuChuong').value * 1
    var contentHTML = ''
    for (var i = 0; i <= 10; i++) {
        var content = `<h3> ${n} x ${i} = ${n * i}</h3>`
        contentHTML += content
    }

    document.getElementById('bangCuuChuong').innerHTML = contentHTML
}


// Bài 8
var players = [[], [], [], []];

var cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S",
    "AS", "7H", "9K", "10D"]

function chiaBai() {
    var contentHTML = ''
    for (var i = 0; i < players.length; i++) {
        for (var j = i; j < cards.length; j += 4) {
            players[i].push(cards[j])
        }
        var content = `<h3>Player ${i + 1}: [${players[i]}]</h3>`
        contentHTML += content

    }
    document.getElementById('chiaBai').innerHTML = contentHTML

}

// Bài 9

// function timSoGaVaSoCho() {
//     var soCon = document.getElementById('inputTongSoCon').value * 1
//     var soChan = document.getElementById('inputTongSoChan').value * 1

//     if(soChan % 2 !==0){

//     }
// }

// Bài 10
function timGocLech() {
    var soGio = document.getElementById('inputGio').value * 1
    var soPhut = document.getElementById('inputPhut').value * 1

    if (soGio > 24 || soPhut > 60) {
        document.getElementById('gocLech').innerHTML = `<h3 class="bg-warning text-white">Số giờ hoặc số phút không hợp lệ!</h3>`
    }
}