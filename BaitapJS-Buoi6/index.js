function domId(id) {
    return document.getElementById(id)
}

// Bài 1
function timSoNguyenDuongNhoNhat() {
    event.preventDefault();
    var sum = 0
    var number = 0
    var result = domId('timsonguyenduong')
    for (var i = 0; sum < 10000; i++) {
        number++
        sum += number
    }


    result.innerHTML = `<h2>Số nguyên dương nhỏ nhất: ${number}</h2>`
}

// Bài 2



function tinhTong() {
    event.preventDefault()
    var soX = domId('soX').value * 1
    var soN = domId('soN').value * 1
    sum = 0
    for (var i = 1; i <= soN; i++) {
        sum += Math.pow(soX, i)
    }
    var result = domId('tinhTong').innerHTML = `<h2>Tổng: ${sum}</h2>`
}

// Bài 3
function tinhGiaiThua() {
    event.preventDefault()
    var n = domId('soGiaiThua').value * 1
    var factorial = 1
    for (var i = 1; i <= n; i++) {
        factorial *= i
    }
    var result = domId('tinhGiaiThua').innerHTML = `<h2>Giai thừa: ${factorial}</h2>`

}

// Bài 4
function taoTheDiv() {
    event.preventDefault()
    var content = ''
    for (var i = 1; i <= 10; i++) {
        if (i % 2 == 0) {
            content += '<p class="bg-primary text-white my-0 p-2">Div chẵn</p>'
        } else {
            content += '<p class="bg-danger text-white my-0 p-2">Div lẻ</p>'

        }
    }

    domId('taoTheDiv').innerHTML = content

}
