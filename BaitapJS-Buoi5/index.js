// Bài 1

function tinhDiemkhuVuc(khuvuc) {
    switch (khuvuc) {
        case 1: {
            return 2
        }
        case 2: {
            return 1
        }
        case 3: {
            return 0.5
        }
        default: {
            return 0
        }
    }
}

function tinhDiemdoiTuong(doituong) {
    switch (doituong) {
        case 1: {
            return 2.5
        }
        case 2: {
            return 1.5
        }
        case 3: {
            return 1
        }
        default: {
            return 0
        }
    }
}

function tinhDiemThi() {
    event.preventDefault()
    var khuVucOption = document.getElementById('khuvuc').value * 1
    var khuVuc = tinhDiemkhuVuc(khuVucOption)

    var doiTuongOption = document.getElementById('doituong').value * 1
    var doiTuong = tinhDiemdoiTuong(doiTuongOption)

    var diemThi1 = document.getElementById('diemthi1').value * 1
    var diemThi2 = document.getElementById('diemthi2').value * 1
    var diemThi3 = document.getElementById('diemthi3').value * 1
    var diemChuan = document.getElementById('diemchuan').value * 1

    var result = document.getElementById('tinhdiemthi')
    var total = diemThi1 + diemThi2 + diemThi3 + khuVuc + doiTuong

    if (diemChuan > 30 || diemThi1 > 10 || diemThi2 > 10 || diemThi3 > 10) {
        return result.innerHTML = `<h2 class="text-warning">Điểm không hợp lệ!!Vui lòng nhập lại</h2>`
    }

    if (diemThi1 == 0 || diemThi2 == 0 || diemThi3 == 0) {
        result.innerHTML = `<h2 class="text-danger">Bạn đã rớt!!! Do có điểm bằng 0</h2>`
    } else if (total < diemChuan) {
        result.innerHTML = `<h2 class="text-danger">Bạn đã rớt!!! Tổng điểm: ${total}</h2>`

    } else {
        result.innerHTML = `<h2 class="text-success">Bạn đã đậu!!! Tổng điểm: ${total}</h2>`

    }
}

// Bài 2
function tinhTienDien() {
    event.preventDefault()
    var ten = document.getElementById('ten').value;
    var soDien = document.getElementById('soDien').value * 1
    var result = document.getElementById('tinhtiendien')
    var giaTienDien50kwDauTien = 500
    var giaTienDien50kwKe = 650
    var giaTienDien100kwKe = 850
    var giaTienDien150kwKe = 1100
    var conlai = 1300
    var tienDien = 0
    if (soDien <= 50) {
        tienDien = giaTienDien50kwDauTien * soDien
    } else if (soDien > 50 && soDien <= 100) {
        tienDien = giaTienDien50kwDauTien * 50 + (soDien - 50) * giaTienDien50kwKe
    } else if (soDien > 100 && soDien <= 200) {
        tienDien = (giaTienDien50kwDauTien * 50 + 50 * giaTienDien50kwKe) + (soDien - 100) * giaTienDien100kwKe
    } else if (soDien > 200 && soDien <= 350) {
        tienDien = (giaTienDien50kwDauTien * 50 + 50 * giaTienDien50kwKe + 100 * giaTienDien100kwKe) + (soDien - 200) * giaTienDien150kwKe

    } else {
        tienDien = (giaTienDien50kwDauTien * 50 + 50 * giaTienDien50kwKe + 100 * giaTienDien100kwKe + 150 * giaTienDien150kwKe) + (soDien - 350) * conlai

    }
    result.innerHTML = `<h2>Họ tên: ${ten}, Tiền điện:${tienDien}</h2>`

}

