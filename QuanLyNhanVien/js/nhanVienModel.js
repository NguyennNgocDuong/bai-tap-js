function NhanVien(maNV, hoTen, email, matKhau, date, luong, chucVu, gioLam) {
    this.ma = maNV;
    this.hoTen = hoTen;
    this.email = email;
    this.matKhau = matKhau;
    this.date = date;
    this.luong = luong;
    this.chucVu = chucVu;
    this.gioLam = gioLam;
    this.tongLuong = function () {

        if (this.chucVu == 'Sếp') {
            return this.luong * 3
        } else if (this.chucVu == 'Trưởng phòng') {
            return this.luong * 2
        } else {
            return this.luong
        }
    }
    this.xepLoai = function () {
        if (this.gioLam >= 192) {
            return 'Nhân viên xuất sắc'
        } else if (this.gioLam >= 176) {
            return 'Nhân viên giỏi'
        } else if (this.gioLam >= 160) {
            return 'Nhân viên khá'
        } else {
            return 'Nhân viên trung bình'
        }
    }
}