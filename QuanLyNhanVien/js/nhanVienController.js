function layThongTinNhanVien() {
  var maNV = document.getElementById("manv").value.trim();
  var hoTen = document.getElementById("name").value.trim();
  var email = document.getElementById("email").value.trim();
  var matKhau = document.getElementById("password").value.trim();
  var date = document.getElementById("datepicker").value;
  var luong = document.getElementById("luongCB").value * 1;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value * 1;

  var nv = new NhanVien(
    maNV,
    hoTen,
    email,
    matKhau,
    date,
    luong,
    chucVu,
    gioLam
  );
  return nv;
}

function renderThongTinNhanVien(arr) {
  var contentHTML = "";
  var format = new Intl.NumberFormat("vn-VN", {
    style: "currency",
    currency: "VND",
  });
  arr.map((nhanvien, index) => {
    var content = `<tr>
        <td>${nhanvien.ma}</td>
        <td>${nhanvien.hoTen}</td>
        <td>${nhanvien.email}</td>
        <td>${nhanvien.date}</td>
        <td>${nhanvien.chucVu}</td>
        <td>${format.format(nhanvien.tongLuong())}</td>
        <td>${nhanvien.xepLoai()}</td>
        <td>
       
        <button type="button" onclick="suaNV(${
          nhanvien.ma
        })" class="btn btn-primary" data-toggle="modal" data-target="#editModal">
       Sửa
       </button>
       <div class="modal fade" id="editModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <header class="head-form mb-0">
            <h2 id="header-title">Log In</h2>
          </header>

          <!-- Modal Header -->
          <!-- 	<div class="modal-header">
					<h4 class="modal-title" id="modal-title">Modal Heading</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div> -->

          <!-- Modal body -->
          <div class="modal-body">
            <form role="form">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"
                      ><i class="fa fa-user"></i
                    ></span>
                  </div>
                  <input
                    type="text"
                    name="tk"
                    id="editmanv"
                    class="form-control input-sm"
                    placeholder="Mã nhân viên"
                  />
                </div>

                <span class="sp-thongbao" id="edittbTKNV"></span>
              </div>

              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"
                      ><i class="fa fa-address-book"></i
                    ></span>
                  </div>
                  <input
                    type="name"
                    name="name"
                    id="editname"
                    class="form-control input-sm"
                    placeholder="Họ và tên"
                  />
                </div>
                <span class="sp-thongbao" id="edittbTen"></span>
              </div>

              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"
                      ><i class="fa fa-envelope"></i
                    ></span>
                  </div>
                  <input
                    type="email"
                    name="email"
                    id="editemail"
                    class="form-control input-sm"
                    placeholder="Email"
                  />
                </div>

                <span class="sp-thongbao" id="edittbEmail"></span>
              </div>

              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"
                      ><i class="fa fa-key"></i
                    ></span>
                  </div>
                  <input
                    type="password"
                    name="password"
                    id="editpassword"
                    class="form-control input-sm"
                    placeholder="Mật khẩu"
                  />
                </div>
                <span class="sp-thongbao" id="edittbMatKhau"></span>
              </div>

              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"
                      ><i class="fa fa-calendar"></i
                    ></span>
                  </div>
                  <input
                    type="text"
                    name="ngaylam"
                    id="editdatepicker"
                    class="form-control"
                    placeholder="Ngày làm"
                  />
                </div>

                <span class="sp-thongbao" id="edittbNgay"></span>
              </div>

              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"
                      ><i class="fa fa-money" aria-hidden="true"></i
                    ></span>
                  </div>
                  <input
                    type="text"
                    name="luongCB"
                    id="editluongCB"
                    class="form-control input-sm"
                    placeholder="Lương cơ bản"
                  />
                </div>
                <span class="sp-thongbao" id="edittbLuongCB"></span>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"
                      ><i class="fa fa-briefcase"></i
                    ></span>
                  </div>
                  <select class="form-control" id="editchucvu">
                    <option>Chọn chức vụ</option>
                    <option>Sếp</option>
                    <option>Trưởng phòng</option>
                    <option>Nhân viên</option>
                  </select>
                </div>

                <span class="sp-thongbao" id="edittbChucVu"></span>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"
                      ><i class="fa fa-clock-o" aria-hidden="true"></i
                    ></span>
                  </div>
                  <input
                    type="text"
                    name="gioLam"
                    id="editgioLam"
                    class="form-control input-sm"
                    placeholder="Giờ làm"
                  />
                </div>
                <span class="sp-thongbao" id="edittbGiolam"></span>
              </div>
            </form>
          </div>

          <!-- Modal footer -->
          <div class="modal-footer" id="editmodal-footer">
            <button
              id="editbtnCapNhatNV"
              type="button"
              class="btn btn-success"
              onclick="capNhatNV()"
              

            >
              Cập nhật
            </button>
            <button
              id="editbtnDong"
              type="button"
              class="btn btn-danger"
              data-dismiss="modal"
            >
              Đóng
            </button>
          </div>
        </div>
      </div>
    </div>
      
       <!-- Button trigger modal -->
       <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#${
         nhanvien.ma
       }">
       Xóa
       </button>
       
       <!-- Modal xóa nhân viên -->
       <div class="modal fade" id="${
         nhanvien.ma
       }" tabindex="-1" role="dialog" aria-labelledby="${
      nhanvien.ma
    }Label" aria-hidden="true">
         <div class="modal-dialog" role="document">
           <div class="modal-content">
             <div class="modal-header">
               <h5 class="modal-title" id="${
                 nhanvien.ma
               }Label">Xóa nhân viên</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
               </button>
             </div>
             <div class="modal-body">
              Bạn có chắc muốn xóa không?
             </div>
             <div class="modal-footer">
             <button type="button" class="btn btn-primary"  data-dismiss="modal" onclick="xoaNV(${
               nhanvien.ma
             })">Có</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Không</button>
             </div>
           </div>
         </div>
       </div>
        </td>
        </tr>`;
    contentHTML += content;
  });
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemNV() {
  var searchValue = document.getElementById("searchName").value;
  var userSearch = listNV.filter((item) => {
    return item.hoTen.toUpperCase().includes(searchValue.toUpperCase());
  });
  renderThongTinNhanVien(userSearch);
}

function timKiemViTri(id, arr) {
  return arr.findIndex((nv) => {
    return nv.ma == id;
  });
}
function layThongTinNhanVienEditModal() {
  var maNV = document.getElementById("editmanv").value.trim();
  var hoTen = document.getElementById("editname").value.trim();
  var email = document.getElementById("editemail").value.trim();
  var matKhau = document.getElementById("editpassword").value.trim();
  var date = document.getElementById("editdatepicker").value;
  var luong = document.getElementById("editluongCB").value;
  var chucVu = document.getElementById("editchucvu").value;
  var gioLam = document.getElementById("editgioLam").value;

  var nv = new NhanVien(
    maNV,
    hoTen,
    email,
    matKhau,
    date,
    luong,
    chucVu,
    gioLam
  );
  return nv;
}
function showThongTinLenForm(nv) {
  document.getElementById("editmanv").value = nv.ma;
  document.getElementById("editname").value = nv.hoTen;
  document.getElementById("editemail").value = nv.email;
  document.getElementById("editpassword").value = nv.matKhau;
  document.getElementById("editdatepicker").value = nv.date;
  document.getElementById("editluongCB").value = nv.luong;
  document.getElementById("editchucvu").value = nv.chucVu;
  document.getElementById("editgioLam").value = nv.gioLam;
}

function sortMaNV(nv1, nv2) {
  return nv1.ma - nv2.ma;
}
