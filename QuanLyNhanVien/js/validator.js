var validator = {
    kiemTraChuoiSo: function (valueInput, IdError, min, max) {
        var idError = document.getElementById(IdError);

        if (valueInput.length < min || valueInput.length > max) {
            idError.innerHTML = `Mã nhân viên từ ${min} - ${max} ký tự`
            return false
        } else {
            idError.innerHTML = ''


            return true
        }
    },
    kiemTraRong: function (valueInput, IdError) {
        if (valueInput == '') {
            document.getElementById(IdError).innerHTML = `Trường này không được rỗng`
            return false
        } else {
            document.getElementById(IdError).innerHTML = ''
            return true
        }

    },
    kiemTraChu: function (valueInput, IdError) {
        var regex = /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/

        if (regex.test(valueInput)) {
            document.getElementById(IdError).innerHTML = ''
            return true
        } else {
            document.getElementById(IdError).innerHTML = `Trường này chỉ được nhập chữ`
            return false
        }
    },
    kiemTraEmail: function (valueInput, IdError) {
        var regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
        if (regexEmail.test(valueInput)) {
            document.getElementById(IdError).innerHTML = ''

            return true
        } else {
            document.getElementById(IdError).innerHTML = `Trường này phải là email`

            return false
        }

    },
    kiemTraMatKhau: function (valueInput, IdError) {
        var regexPass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/
        if (regexPass.test(valueInput)) {
            document.getElementById(IdError).innerHTML = ''
            return true
        } else {
            document.getElementById(IdError).innerHTML = `Mật khẩu phải từ 6-10 ký tự và chứa 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt và `
            return false
        }
    },
    kiemTraLuong: function (valueInput, IdError) {
        if (valueInput < 1000000 || valueInput > 20000000) {
            document.getElementById(IdError).innerHTML = `Lương phải từ 1.000.000 - 20.000.000`
            return false
        } else {
            document.getElementById(IdError).innerHTML = ''
            return true
        }
    },
    kiemTraChucVu: function (valueInput, IdError) {
        if (valueInput == 'Chọn chức vụ') {
            document.getElementById(IdError).innerHTML = `Vui lòng chọn chức vụ`
            return false
        } else {
            document.getElementById(IdError).innerHTML = ''
            return true
        }
    },
    kiemTraGioLam: function (valueInput, IdError) {
        if (valueInput < 80 || valueInput > 200) {
            document.getElementById(IdError).innerHTML = `Số giờ làm từ 80 - 200`
            return false
        } else {
            document.getElementById(IdError).innerHTML = ''
            return true
        }
    },
    kiemTraTrung: function (maNV, listNV) {
        var index = listNV.findIndex(nv => {
            return nv.ma == maNV
        })
        if (index !== -1) {
            document.getElementById('tbTKNV').innerHTML = 'Mã nhân viên đã tôn tại'
            return false
        } else {
            document.getElementById('tbTKNV').innerHTML = ''

            return true

        }
    },
}